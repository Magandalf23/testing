ai_historical_focus_list_BUL = {
    ai_national_focuses = {
        # grab as fast as you can if available
BUL_Highway_Development
BUL_Begin_2_Year_Plan
BUL_Build_It_Better
BUL_Heavy_Machines
BUL_Fuel_Our_Industry
BUL_Finnish_2_Year_Plan
BUL_Arms_Expansion
BUL_Efficient_Designs
BUL_War_Production
BUL_Fuel_Reserves
BUL_Monarch_Speech
BUL_Radar_Testing
BUL_University_Of_Sofia
BUL_Geological_Survey
BUL_Dig_Deep
BUL_Expand_Western_Mines
bul_naval_effort
    }
}