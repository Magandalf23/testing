focus_tree = {
	id = ven_focus
	
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = VEN
		
		}
	}
	
	
		default = no
     	
		focus = { 
		id = VEN_remove_Atlantikwall
		icon = GFX_goal_generic_SS_focus
		available = {
		GER={has_completed_focus=GER_atlantikwall OR={date>1942.4.4 NOT={448={is_controlled_by = ITA}}}}
			
		
		
		}
		
		x = 1
		y = 1
		cost = 5
		
	completion_reward = {
	
	 GER={FRA={ every_owned_state= {
               limit={is_coastal=yes is_on_continent = europe }
                add_state_modifier = {
                    modifier = { max_dig_in=-7 }              
                }}}
		every_owned_state= {
               limit={is_coastal=yes is_on_continent = europe  OR={is_core_of=HOL is_core_of=BEL is_core_of=DEN}}
                add_state_modifier = {
                    modifier = { max_dig_in=-7 }              
                }}}
	
	}	
	}
		focus = {
		id = VEN_hurensohn_sealion
		icon = GFX_goal_generic_consumer_goods
	
		x = 10
		y = 0
		cost = 1
        ai_will_do = {
			factor = 1
		}	
		available_if_capitulated = yes
       	
	available = { 
		or = {
		ENG = {NOT = {has_full_control_of_state = 126 }}
		ENG = {NOT = {has_full_control_of_state = 127 }}
		ENG = {NOT = {has_full_control_of_state = 123 }}
		ENG = {NOT = {has_full_control_of_state = 338 }}
		ENG = {NOT = {has_full_control_of_state = 129 }}
		}
	
	}
	
		completion_reward = {
		
		
		CAN = {add_ideas =  tot_economic_mobilisation}
		SAF = {add_ideas =  tot_economic_mobilisation}
		AST = {add_ideas =  tot_economic_mobilisation}
		RAJ = {add_ideas =  tot_economic_mobilisation}
		ENG = {add_ideas =  tot_economic_mobilisation}
		USA = {add_ideas =  tot_economic_mobilisation}
		FRA = {add_ideas =  tot_economic_mobilisation}
		NZL = {add_ideas =  tot_economic_mobilisation}
		USA = { remove_ideas = USA_dont_invade_free_europe }
		
		
		
   }
		
	
		
	}
