heavy_water_infiltration = {
	name = heavy_water_infiltration
	desc = heavy_water_infiltration_desc
	outcome = heavy_water_infiltration_outcome
	icon = "GFX_phase_heavy_water_infiltration_small"
	picture = "GFX_phase_heavy_water_infiltration"
	
	equipment = {
	}
}

heavy_water_attack = {
	name = heavy_water_attack
	desc = heavy_water_attack_desc
	outcome = heavy_water_attack_outcome
	icon = "GFX_phase_heavy_water_attack_small"
	picture = "GFX_phase_heavy_water_attack"
}

ant_1 = {
	name = ant_1_n
	desc = ant_1_d
	outcome = heavy_water_attack_outcome
	icon = "GFX_phase_bruneval_infiltration_small"
	picture = "GFX_phase_bruneval_infiltration"
}

himm_1 = {
	name = h1
	desc = himm_1
	outcome = heavy_water_attack_outcome
	icon = "GFX_phase_bruneval_infiltration_small"
	picture = "GFX_phase_bruneval_infiltration"
}

ant_2 = {
	name = ant_2_n
	desc = ant_2_d
	outcome = heavy_water_attack_outcome
	icon = "GFX_phase_lar_fra_coup_launch_coup_small"
	picture = "GFX_phase_lar_fra_launch_coup"
}

himm_2 = {
	name = h2
	desc = himm_2
	outcome = heavy_water_attack_outcome
	icon = "GFX_phase_lar_fra_coup_launch_coup_small"
	picture = "GFX_phase_lar_fra_launch_coup"
}

ant_3 = {
	name = ant_3_n
	desc = ant_3_d
	outcome = heavy_water_attack_outcome
	icon = "GFX_phase_fake_intel_plant_evidence_small"
	picture = "GFX_phase_collaboration_government_train_paramilitary_forces"
}

himm_3 = {
	name = h3
	desc = himm_3
	outcome = heavy_water_attack_outcome
	icon = "GFX_phase_fake_intel_plant_evidence_small"
	picture = "GFX_phase_collaboration_government_train_paramilitary_forces"
}

heavy_water_exfiltration = {
	name = heavy_water_exfiltration
	desc = heavy_water_exfiltration_desc
	outcome = heavy_water_exfiltration_outcome
	risk_extra = heavy_water_exfiltration_outcome_fail
	icon = "GFX_phase_heavy_water_exfiltration_small"
	picture = "GFX_phase_heavy_water_exfiltration"
}

bruneval_infiltration = {
	name = bruneval_infiltration
	desc = bruneval_infiltration_desc
	outcome = bruneval_infiltration_outcome
	icon = "GFX_phase_bruneval_infiltration_small"
	picture = "GFX_phase_bruneval_infiltration"
	
	equipment = {
	}
}

bruneval_attack = {
	name = bruneval_attack
	desc = bruneval_attack_desc
	outcome = bruneval_attack_outcome
	icon = "GFX_phase_bruneval_attack_small"
	picture = "GFX_phase_bruneval_attack"
}

bruneval_exfiltration = {
	name = bruneval_exfiltration
	desc = bruneval_exfiltration_desc
	outcome = bruneval_exfiltration_outcome
	risk_extra = bruneval_exfiltration_outcome_fail
	icon = "GFX_phase_bruneval_exfiltration_small"
	picture = "GFX_phase_bruneval_exfiltration"

	requirements = {
		has_equipment = {
			convoy > 0
		}
	}
	equipment = {
		
	}
}

capture_tito_infiltration = {
	name = capture_tito_infiltration
	desc = capture_tito_infiltration_desc
	icon = "GFX_phase_capture_tito_infiltration_small"
	picture = "GFX_phase_capture_tito_infiltration"
	
	equipment = {
	}
}

capture_tito_attack = {
	name = capture_tito_attack
	desc = capture_tito_attack_desc
	outcome = capture_tito_attack_outcome
	icon = "GFX_phase_capture_tito_attack_small"
	picture = "GFX_phase_capture_tito_attack"
}

capture_tito_exfiltration = {
	name = capture_tito_exfiltration
	desc = capture_tito_exfiltration_desc
	icon = "GFX_phase_capture_tito_exfiltration_small"
	picture = "GFX_phase_capture_tito_exfiltration"

	equipment = {
		
	}
}

rescue_mussolini_infiltration = {
	name = rescue_mussolini_infiltration
	desc = rescue_mussolini_infiltration_desc
	icon = "GFX_phase_capture_tito_infiltration_small"
	picture = "GFX_phase_capture_tito_infiltration"
	
	equipment = {
	}
}

rescue_mussolini_attack = {
	name = rescue_mussolini_attack
	desc = rescue_mussolini_attack_desc
	outcome = capture_tito_attack_outcome
	icon = "GFX_phase_rescue_mussolini_attack_small"
	picture = "GFX_phase_rescue_mussolini_attack"
}

rescue_mussolini_exfiltration = {
	name = rescue_mussolini_exfiltration
	desc = rescue_mussolini_exfiltration_desc
	icon = "GFX_phase_rescue_mussolini_exfiltration_small"
	picture = "GFX_phase_rescue_mussolini_exfiltration"

	equipment = {
		
	}
}
